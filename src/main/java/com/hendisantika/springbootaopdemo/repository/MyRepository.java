package com.hendisantika.springbootaopdemo.repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:42
 */
public interface MyRepository {

    String getSomething(int id);
}