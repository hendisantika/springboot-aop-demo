package com.hendisantika.springbootaopdemo.repository;

import org.springframework.stereotype.Repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:43
 */
@Repository
public class MyRepositoryOne implements MyRepository {

    @Override
    public String getSomething(int id) {
        return "Repo one with id: " + id;
    }
}