package com.hendisantika.springbootaopdemo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:48
 */
@Slf4j
@Aspect
@Component
public class MyAfterThrowingAspect {

    @AfterThrowing(
            value = "com.hendisantika.springbootaopdemo.aspect.JoinPoints.controllerLayer()",
            throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, IllegalArgumentException ex) {
        log.info("ASPECT - After Throwing: exception \"{}\" on controller \"{}\"", ex, joinPoint.getSignature());
    }
}