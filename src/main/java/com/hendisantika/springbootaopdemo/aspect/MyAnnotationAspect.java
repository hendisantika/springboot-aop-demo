package com.hendisantika.springbootaopdemo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:49
 */
@Slf4j
@Aspect
@Component
public class MyAnnotationAspect {

    @Around("@annotation(com.hendisantika.springbootaopdemo.annotation.TrackTime)")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        log.info("ASPECT - Around: Time before proceed: {}", startTime);

        Object proceed = joinPoint.proceed();

        long totalTime = System.currentTimeMillis() - startTime;
        log.info("ASPECT - Around: Time after proceed {}: {}", joinPoint.getSignature(), totalTime);

        return proceed;
    }

}