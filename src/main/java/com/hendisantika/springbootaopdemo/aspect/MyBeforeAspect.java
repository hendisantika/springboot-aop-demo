package com.hendisantika.springbootaopdemo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:49
 */
@Slf4j
@Aspect
@Component
public class MyBeforeAspect {

    @Before("com.hendisantika.springbootaopdemo.aspect.JoinPoints.controllerLayer()")
    public void beforeSomething(JoinPoint joinPoint) {
        log.info("ASPECT - Before: {}", joinPoint.getSignature());
    }
}