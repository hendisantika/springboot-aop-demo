package com.hendisantika.springbootaopdemo.aspect;

import org.aspectj.lang.annotation.Pointcut;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:42
 */
public class JoinPoints {
    @Pointcut("execution(* com.hendisantika.springbootaopdemo.service.*.*(..))")
    public void serviceLayer() {
    }

    @Pointcut(
            value = "execution(* com.hendisantika.springbootaopdemo.repository.*.*(..)) && args(id)",
            argNames = "id")
    public void repositoryLayer(int id) {
    }

    @Pointcut("execution(* com.hendisantika.springbootaopdemo.controller.*.*(..))")
    public void controllerLayer() {
    }
}
