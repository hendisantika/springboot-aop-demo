package com.hendisantika.springbootaopdemo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:46
 */
@Slf4j
@Aspect
@Component
public class MyAfterAspect {

    @After(value = "com.hendisantika.springbootaopdemo.aspect.JoinPoints.repositoryLayer(id)")
    public void afterSomething(JoinPoint joinPoint, int id) {
        log.info("ASPECT - After: {}", joinPoint.getSignature());
    }

    @AfterReturning(
            value = "com.hendisantika.springbootaopdemo.aspect.JoinPoints.repositoryLayer(id)",
            returning = "result")
    public void afterReturning(JoinPoint joinPoint, Object result, int id) {
        log.info("ASPECT - After Returning: repository {} with id {} returned {}", joinPoint, id, result);
    }
}