package com.hendisantika.springbootaopdemo.service;

import com.hendisantika.springbootaopdemo.repository.MyRepositoryTwo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:44
 */
@Slf4j
@AllArgsConstructor
@Service
public class MyServiceTwo implements MyService {

    private final MyRepositoryTwo repositoryTwo;

    @Override
    public String getSomething(int id) {
        return repositoryTwo.getSomething(id);
    }
}