package com.hendisantika.springbootaopdemo.service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:44
 */
public interface MyService {
    String getSomething(int id) throws InterruptedException;
}
