package com.hendisantika.springbootaopdemo.service;

import com.hendisantika.springbootaopdemo.annotation.TrackTime;
import com.hendisantika.springbootaopdemo.repository.MyRepositoryOne;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:44
 */
@Slf4j
@AllArgsConstructor
@Service
public class MyServiceOne implements MyService {

    private final MyRepositoryOne repositoryOne;

    @TrackTime
    @Override
    public String getSomething(int id) throws InterruptedException {
        Thread.sleep(2000);
        return repositoryOne.getSomething(id);
    }
}