package com.hendisantika.springbootaopdemo.controller;

import com.hendisantika.springbootaopdemo.service.MyServiceOne;
import com.hendisantika.springbootaopdemo.service.MyServiceTwo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-aop-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-01-21
 * Time: 13:46
 */
@Slf4j
@RestController
@AllArgsConstructor
public class MyAopController {

    private final MyServiceOne serviceOne;

    private final MyServiceTwo serviceTwo;

    @GetMapping("/api/one/{id}")
    public String getOne(@PathVariable("id") int id) throws InterruptedException {

        if (id == 0) {
            throw new IllegalArgumentException("id value can't be 0");
        }
        return serviceOne.getSomething(id);
    }

    @GetMapping("/api/two/{id}")
    public String getTwo(@PathVariable("id") int id) {
        return serviceTwo.getSomething(id);
    }
}