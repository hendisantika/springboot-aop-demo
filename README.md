# Spring Boot AOP Demo

#### Run this project by these commands :
1. `git clone https://gitlab.com/hendisantika/springboot-aop-demo.git`
2. `cd springboot-aop-demo`
3. Run the app by this command : `mvn clean spring-boot:run`